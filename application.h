//
// Created by meow star on 2019/10/3.
//

#ifndef RCMD_RECALL_APPLICATION_H
#define RCMD_RECALL_APPLICATION_H
#define CPPHTTPLIB_REQUEST_URI_MAX_LENGTH 1024*1024*1024
#include <recall_manager.h>
#include <thread>
#include "plog/Log.h"
#include <cpp-httplib/httplib.h>
using namespace std;
class Application {
public:
    Application(){
        _repository_thread = NULL;
        _w2v = NULL;
        _repository_thread_flag = 1;
    }
    int server_thread(string localip, int port);

    //void do_related_recall(const httplib::Request &req, httplib::Response &res);
    void do_strategy_recall(const httplib::Request &req, httplib::Response &res);
    void do_param_recall(const httplib::Request& req, httplib::Response& res);
    void do_get_records_v2(const httplib::Request& req, httplib::Response& res);
    void do_get_repo_status(const httplib::Request& req, httplib::Response& res);
    void do_get_records(const httplib::Request& req, httplib::Response& res);
    void do_ping(const httplib::Request& req, httplib::Response& res);
    void build_json_from_recall(RecallResult& result, json& j);
    int init();
    int init_log();
    int reload_repositorys();
    void repository_thread();
public:
    int _repository_thread_flag;
    json _cfg;
    httplib::Server _svr;
    thread* _repository_thread;
    thread* _server_thread;
    WordVectorLib* _w2v;
    unordered_map<string, RecallManager*> _mp_recall_mgr;
};


#endif //RCMD_RECALL_APPLICATION_H

//
// Created by meow star on 2019/9/25.
//
#ifndef RCMD_RECALL_DEFINE_H
#define RCMD_RECALL_DEFINE_H
#include <stdlib.h>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include "stdio.h"
#include <sys/time.h>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "utils.hpp"
using namespace std;

enum{
FORWARD_IDX_ID = 0,
FORWARD_IDX_SOURCE,
FORWARD_IDX_VIEW_COUNT,
FORWARD_IDX_COMMENT_COUNT,
FORWARD_IDX_SHARE_COUNT,
FORWARD_IDX_DIGG_COUNT,
FORWARD_IDX_PUBLISH_TIME,
FORWARD_IDX_FROM,
FORWARD_IDX_LOCATION,
FORWARD_IDX_TITLE,
FORWARD_IDX_EXTRA,
FORWARD_IDX_COVERAGE,
FORWARD_IDX_CONTENT,
FORWARD_IDX_THUMB,
FORWARD_IDX_SEXY,
FORWARD_IDX_HOT_SCORE,
FORWARD_IDX_PORN_SCORE,
FORWARD_IDX_REAL_CLICK_COUNT,
FORWARD_IDX_TOTAL_DISPLAY_COUNT,
FORWARD_IDX_TOTAL_DURATION,
FORWARD_IDX_KEYWORDS,
FORWARD_IDX_CATEGORYS,
FORWARD_IDX_COUNT,
};


#define CFG_FILE_PATH "./rcmd_recall.json"
#define LOG_FILE_PATH "./log/rcmd_recall.log"
#define LOG_FILE_PATH_SPD "./log/rcmd-recall.log"
#define PROCESS_NAME "rcmd-recall"
#define MODEL_RELOAD_DELAY 1
#define MAX_LIBRARY_COUNT 950000
#define CPPHTTPLIB_TCP_NODELAY true
#define CPPHTTPLIB_RECV_BUFSIZ size_t(1048576u)
#define CPPHTTPLIB_THREAD_POOL_COUNT                                           \
  ((std::max)(64u, std::thread::hardware_concurrency() > 0                      \
                      ? std::thread::hardware_concurrency() * 4                \
                      : 0))
#endif //RCMD_RECALL_DEFINE_H

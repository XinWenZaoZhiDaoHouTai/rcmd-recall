//
// Created by meow star on 2019/10/5.
//

#include "recall_profile.h"
#define PROFILE_ELEM_IDX_FILTER 0
const unordered_set<string>& RecallProfile::filter() {
    return _raw_tuple;
}
int RecallProfile::decode_from_msgpack(const string &bin, RecallProfile &profile) {
    int ret = 0;
    try{
        spdlog::debug("content {0:d} {1:x}", bin.size(),bin.data()[0]);

        msgpack::object_handle oh = msgpack::unpack(bin.data(), bin.size());
        msgpack::object o = oh.get();
        profile._raw_tuple = o.as<unordered_set<string>>();
    }catch(msgpack::unpack_error e){
        spdlog::error("unpack profile failed, length={0:d}, error={1:s}", (int)bin.size(), e.what());
        //PLOG_ERROR << "unpack profile failed, length=" << (int)bin.size() << ", error" << e.what();
        ret  = -1;
    }
    return ret;
}
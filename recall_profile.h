//
// Created by meow star on 2019/10/3.
//

#ifndef RCMD_RECALL_RECALL_PROFILE_H
#define RCMD_RECALL_RECALL_PROFILE_H

#include <define.h>
#include <msgpack-c/include/msgpack.hpp>
struct RecallProfile{
    vector<float> keyword_vector;
    const unordered_set<string>& filter();
    size_t count(){return _raw_tuple.size();}
    static int decode_from_msgpack(const string& bin, RecallProfile& profile);
private:

    unordered_set<string> _raw_tuple;
};



#endif //RCMD_RECALL_RECALL_PROFILE_H

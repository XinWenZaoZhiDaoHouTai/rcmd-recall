//
// Created by meow star on 2019/9/25.
//

#ifndef RCMD_RECALL_WORD2VEC_H
#define RCMD_RECALL_WORD2VEC_H


#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <string>
#include <math.h>
using namespace std;
class MathUtils{
public:
    static float get_mold(const vector<float>& vec){   //求向量的模长
        int n = vec.size();
        float sum = 0.0;
        for (int i = 0; i<n; ++i)
            sum += vec[i] * vec[i];
        return sqrt(sum);
    }

    static float get_cosine_similarity(const vector<float>& lhs, const vector<float>& rhs){
        int n = lhs.size();
        float tmp = 0.0;  //内积
        for (int i = 0; i<n; ++i)
            tmp += lhs[i] * rhs[i];
        return tmp / (get_mold(lhs)*get_mold(rhs));
    }

    static int add_vector(vector<float>& a, const vector<float>& add, float multiply = 1){
        if (a.size() != add.size()){
            return -1;
        }
        for(size_t i = 0; i < a.size(); i ++){
            a[i] += add[i] * multiply;
        }
        return 0;
    }

    static void vector_multiply(vector<float>& a, float b){
        for(size_t i = 0; i < a.size(); i ++){
            a[i] *= b;
        }
    }
    };

class WordVectorLib{
public:
    WordVectorLib(string w2v_path){
        _path = w2v_path;
        _vec_len = 0;
    }
    int load_vectors(unordered_map<string, int>& keywords);
    vector<float>* get_vector(const string& key);
    int calc_keywords_vector(const vector<string>& keywords, vector<float>& result);
    int calc_keywords_vector(const unordered_map<string, float>& keywords, vector<float>& result);
public:
    unordered_map<string, vector<float>> _mp_w2v;
    string _path;
    uint32_t _vec_len;
};
#endif //RCMD_RECALL_WORD2VEC_H
